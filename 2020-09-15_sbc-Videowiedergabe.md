# Videowiedergabe auf verschiedenen Computern

## Anforderungen
Ich wurde von Künstlern gefragt, wie sie am einfachsten ein Video auf einem Fernseher wiedergeben können. Das Video sollte als immer wieder wiederholt werden.

Meine Idee war, entweder USB-Stick an den SmartTV und im Fernseher eine passende Option suchen oder einen Minicomputer ran hängen, der die passenden Einstellungen gesetzt hat. Ich habe versucht die zweite Idee umzusetzen.

## rPi (allererste Generation)
* offizielle Linux-Distro aufspielen
* starten, youtube-dl per pip3 installieren, Video runter laden
* h264 fullHD Video mit omxplayer flüssig wiedergegeben bekommen
* ich brauche keinen Xorg & war nach spätestens 30 Minuten fertig

## Cubieboard
* mit armbian versucht
* nach 3 Stunden gescheitert, weil ich kein Xorg zum laufen bekomme

## pine64
* armbian buster ist zu alt für Videobeschleunigung
* dist-upgrade auf bullseye
* armbian bullseye schafft auch keine Wiedergabe ohne framedrops
* Mehrere Stunden Zeitaufwand, 

## Asus Netbook mit AMD C-60
* endlich wieder vanilla debian
* Wiedergabe mit Standardeinstellungen von mpv mit framedrops
* "mpv -vo vdpau" wirkt flüssiger und zeigt auch keine framedrops mehr an
* 1 Stunde Zeitaufwand (uA weil normaler debian installer statt "fertig" eingerichtetes Abbild für eine SD-Karte)

## Zusammenfassung
Nachdem ich die RaspberryPi Foundation jahrelang versuche zu vermeiden, scheinen die mittlerweile doch ganz brauchbare Arbeit zu machen. Am Anfang hatte ich sehr stark das Gefühl, dass die Leute hinter dem rPi hauptsächlich Marketing-Menschen sind. Dass das rPi verbreitet ist, weil es verbreitet ist, ist meinem Vertrauen in die Hardware auch nicht gerade förderlich. Schließlich ist Microsoft Windows auch verbreitet, weil es verbreitet ist. Nach einem guten Betriebssystem wirkt es für mich höchst selten.

Alles sehr unbefriedigend.
