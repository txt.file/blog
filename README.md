<!-- SPDX-License-Identifier: ISC -->
<!-- SPDX-FileCopyrightText: 2021 Vieno Hakkerinen <vieno@hakkerinen.eu> -->
# blog

Until I have a proper blog engine I will put my ramblings here.

## License

All files have a proper (REUSE)[https://reuse.software/] license & copyright header.
