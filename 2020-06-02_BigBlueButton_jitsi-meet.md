<!-- SPDX-License-Identifier: ISC -->
<!-- SPDX-FileCopyrightText: 2021 Vieno Hakkerinen <vieno@hakkerinen.eu> -->
# BigBlueButton oder Jitsi Meet

Aus meiner Erfahrung mit meinem Desktop aus dem Jahre 2011, sage ich: definitiv BigBlueButton.

Das liegt daran, wie BigBlueButton (BBB) und Jitsi Meet jeweils funktionieren.

## BigBlueButton

BigBlueButton ist für Lernsituationen und Ähnliches gedacht. Da kommen schnell mal 30 Leute zusammen. Daher ist BBB für größere Gruppen ausgelegt. Ich war schon ohne Probleme in einer BBB-Konferenz mit ~50 zuhörenden Teilnehmern.

BBB hat deutlich größere Systemanforderungen für den Server.

Der BBB Server bekommt die Videosstreams der Teilnehmer, rechnet die zu einem Videostream zusammen und verschickt einen Videostream pro Teilnehmer. Damit muss jeder Teilnehmer nur noch ein Video dekodieren.

Einbindung von Youtube stellt kein Problem dar und man kann sich währenddessen weiter unterhalten.

## Jitsi Meet

Jitsi Meet ist super für kleinere Treffen. Mein größtes erlebtes Jitsi Meet Treffen war mit ~20 Personen und da hatten dann alle Probleme, bis ein paar Leute gegangen waren und es damit wieder eine kleinere Konferenz war.

Jitsi Meet macht nur eine Vermittlung der Videostreams. Jedes empfangene Video wird an jeden anderen Teilnehmer weitergeschickt. Damit stellt eine Jitsi Meet Videokonferenz mit 10 Teilnehmern die Anforderung 9 Videosstreams zu empfangen und dekodieren und einen Videostream zu kodieren und versenden.

Meine Erfahrung ist weiterhin, dass die Qualitätseinstellungen im Client keinen Einfluss auf die empfangenen Videos haben. Kann es auch nicht, wenn der Server nicht umkodiert.

Es gibt in Jitsi Meet ein paar Optionen um visuelle Gimmicks (zum Beispiel die blauen Punkte an jedem Ton sendenden Teilnehmer) zu deaktivieren. Diese müssen leider auf dem Server eingestellt werden und gelten dann für alle Clients an diesem Server.

Unter den Android- & iOS-Programmen werden von anderen eingebundene Youtube-Videos nicht angezeigt. Aus Nutzersicht sind auf einmal alle im Raum stumm, keine Ahnung warum.

## Haftungsausschluss

Dies stellt mein Verständnis bis zum jetzigen Zeitpunkt (2. Juli 2020) dar. Bitte teilt mir mit, wenn ich etwas falsch dargestellt habe.
