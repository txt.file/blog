<!-- SPDX-License-Identifier: ISC -->
<!-- SPDX-FileCopyrightText: 2021 Vieno Hakkerinen <vieno@hakkerinen.eu> -->
# self hosting at home

## hardware
I started to look into self hosting at home about 10 years ago. The reason was that I do not care if I pay 10 € for a hosting provider or my electrical power provider. On the other hand I care who has access to my stuff. Hosting at home I have a very good overview who can access the hardware. By using a hosting company I have to trust them and can not say who puts fingers on the hardware I use. Back in 2012 there have not been that many single board computers and the few ones haven't been so easy to use. The cost limit of 10 € per month electrical bill I roughly calculated that the computer must need less than 50 W under load. I mostly ignored the initial price of buying the hardware. I ended with an AMD E-350 based mainboard[0], 8 GB ram, 3 1 TB hard drives, a 32 GB SSD[1] and a whooping 350 W power supply. I still use this computer on a daily basis as it is my desktop. A week ago I took my cheap electricity usage monitor and "measured" this computer. I got ~30 W when idle and ~45 W under load. The computer has only one fan in the power supply and is therefore nearly silent.

A few years later single board computers are now normal usable computers and I currently use an A20-OLinuXino-LIME2[2] and my wireless router for my server needs. XMPP with prosody runs well on my wireless router. A bunch of websites run well on the LIME2. 2 nextcloud instances crawl and error on the LIME2. I think about replacing the LIME2 with my pine64+ but have not done it yet. I need to learn much more about server configuration and service movement.

## network requirements

To be able to host stuff at home at least a public legacy-IP (IPv4) address is needed on the internet connection. A current-IP (IPv6) range makes things better. Sadly in Germany 2 of 3 mobile phone networks still do not provide current-IP to the end users. There are many networks ignoring this "new" (22 years old) technology.

Depending on the internet service provider (ISP) the public legacy-IP might change between daily and never. Daily changing addresses are usable but I recommend to use an dyndns provider. I had good experience with afraid.org.[3]

You want to have a domain. It does not really matter if it is your own registered domain or your chosen dyndns provider gives you one. A domain is needed to issue certificates for encrypted communication between client and server.

Important is what data should be transferred in what time. My fullHD videos in good quality have ~15 MBit/s. If I want to stream these videos my upload must be at least this fast. My flac encoded CDs are at ~1 MBit/s.

## security implications

### IP ranges

Your ISP gives you IP addresses. The ISP is able to know which customer has which addresses. With static public addresses yesterdays bad thing and last years bad thing can be put in correlation. On the other hand only the ISP know how long daily changing addresses are stored. Maybe it is stored which customer had which addresses 365 days ago. Maybe it is not stored. Who knows. So it is better to not think that daily changing addressed add significant security. Maybe it gets a bit more difficult for Random Joe but probably not for law enforcement and similar actors.

### TLS

TLS provides two important security features to a connection. Authentication and encryption. The authentication is why TLS only works with domains. The client (hopefully) checks if the domain in the server provided certificate is the domain it connects to. When I access the website on www.example.com and get a certificate for www.example.net my web browser will tell me this with a big warning message.

The encryption is important to get the connection between client and server temper resistant. With good encryption nobody is able to see if a cat picture or the planning of next weeks surprise party are exchanged.

### domain data

Before GDPR most top level domains (TLD) have been publicly query-able who own a domain. Since GDPR nearly all TLDs provide this information only on special request. So it is much harder to get a physical address from the domain. Law enforcement has probably ways to get this data.

### hardware access

With the stuff at home it is pretty easy to tell who has physical access to it.

## conclusion

Always think about your thread model before implementing security features. What should be prevented and how much time/energy/money do I want to invest in preventing this.

I recommend to talk other people about their thread models and their security implementations. I learn much from different opinions.


## links / footnotes
[0] https://www.asus.com/Motherboards/E35M1M_PRO/
[1] SSD were new shiny technology way back when
[2] https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME2/open-source-hardware
[3] https://freedns.afraid.org/
