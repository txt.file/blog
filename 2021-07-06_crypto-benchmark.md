<!-- SPDX-License-Identifier: ISC -->
<!-- SPDX-FileCopyrightText: 2021 Vieno Hakkerinen <vieno@hakkerinen.eu> -->
# 2021-07-06 crypto benchmark on pine64+ (2 GiB model)

* armbian buster updated to debian sid
* 3.5" spinning rust (hard drive) connected via USB2.0


## btrfs without crypto

* btrfs over whole disk mounted with noatime
* `sysbench fileio prepare --file-total-size=10GiB` -> `32.43 MiB/sec`

## btrfs with cryptsetup

* cryptsetup over whole disk, btrfs over whole LUKS mounted with noatime
* `sysbench fileio prepare --file-total-size=10GiB` -> `32.34 MiB/sec`

## zfs without crypto

* `zpool create -R /mnt -o ashift=12 -O atime=off -O compression=off zpool /dev/sda`
* `sysbench fileio prepare --file-total-size=10GiB` -> `17.19 MiB/sec`

## zfs with crypto

* `zpool create -R /mnt -o ashift=12 -O atime=off -O compression=off -O encryption=on -O keyformat=passphrase -O keylocation=prompt zpool /dev/sda`
* `sysbench fileio prepare --file-total-size=10GiB` -> `7.47 MiB/sec`
