# installing Debian buster on a PineBook

If you want to use the notebook as a notebook you better use Debian bullseye. Buster is missing the Lima driver for the Mali GPU and the driver for the cedrus video engine and a bunch of other stuff. For Linux drivers take a look at [linux-sunxi.org](https://linux-sunxi.org/Linux_mainlining_effort#Status_Matrix)

Used Images: [deb.debian.org…buster…firmware.pinebook.img](https://deb.debian.org/debian/dists/buster/main/installer-arm64/20190702+deb10u6/images/netboot/SD-card-images/firmware.pinebook.img.gz) + [deb.debian.org…buster…partition.img](https://deb.debian.org/debian/dists/buster/main/installer-arm64/20190702+deb10u6/images/netboot/SD-card-images/partition.img.gz)

## partitioning
The Debian-installer defaults to GPT for partitioning. Problem is that using GPT results in following error when installing u-boot to eMMC
```
root@host:/# u-boot-install-sunxi64 /dev/mmcblk2
/bin/u-boot-install-sunxi64: device/image (/dev/mmcblk2) uses GPT partition table, unusable on sunxi64
root@host:/# 
```
So we must use msdos partition table. To be able to create a msdos partition table you need to change the debconf priority from ```high``` to ```low```.

u-boot 2020.10+dfsg-1 said
```
=> fstypes
Supported filesystems: fat, ext4
=> 
```
So I need a separate /boot as I want to use btrfs for /.

My partition layout:
```
#1 → 512 MiB → ext2 → /boot
#2 → 4 GiB → swap → none
#3 → remaining → btrfs → /
```

After partitioning you can switch debconf priority back to high.

## boot loader
The Debian-installer does not know how to install a boot loader on a pinebook therefore it installs Debian without a boot loader. As a boot loader is needed to boot Debian my solution is to chroot in the installed system and install u-boot by hand.

Run following commands in the Debian-installer environment to ensure all needed paths are available
```
mount --rbind /dev /target/dev
mount --rbind /proc /target/proc
mount --rbind /sys /target/sys
```

If you have /boot on a separate partition you need to run following commands inside chroot. If /boot is part of / skip these commands.
```syntax=sh
apt install rsync
wget -O /etc/kernel/postinst.d/zz-sync-dtb https://salsa.debian.org/debian/u-boot-menu/-/raw/debian/master/zz-sync-dtb
chmod u+x,g+x,o+x /etc/kernel/postinst.d/zz-sync-dtb
/etc/kernel/postinst.d/zz-sync-dtb $(uname -r)
```

To install u-boot run following commands inside chroot
```syntax=sh
apt --install-recommends --install-suggests install u-boot-menu u-boot-sunxi
u-boot-install-sunxi64 /dev/mmcblk2
u-boot-update
```

## Acknowledgements
Many thanks to Vagrant Cascadian, [#debian-arm](ircs://irc.oftc.net:6697/#debian-arm) and all the other people who helped me over the last year with my PineBook. Many thanks to the Debian project in general. Of course many thanks to the free software community in general.
